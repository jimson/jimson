def uth()
    Rake.sh "git submodule update --remote --init --rebase"
end

begin
    require_relative('core.cook/ruby/base.rb')
rescue LoadError
    uth()
    require_relative("core.cook/ruby/base.rb")
end

require('cook')
require('git')

desc "Update all submodules to their head (if on a branch)"
task :uth do
    Git.uth()
end

namespace :build do
    desc "build everyting"
    task :all do
        Cook.default.build()
    end
end

namespace :ut do
    desc "run all unit tests"
    task :all => "build:all" do
        sh Cook.default.output_dir("ut.all")
    end
end

namespace :gen do

    desc "generate cmake output"
    task :cmake do
        sh Cook.default.cmd(["-g cmake"])
    end
    
    desc "generate html output"
    task :html do
        sh Cook.default.cmd(["-g html"])
    end
end

task :default do
    sh "rake -T"
end

